import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import Home from '../pages/admin/Admin.vue'
import Employees from '../pages/admin/Employees.vue'
import EmployeeCreate from '../pages/admin/EmployeeCreate.vue'
import Login from '../pages/admin/Login.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/home',
    component: Employees
  },
  {
    path: '/add',
    component: EmployeeCreate
  },
  {
    path: '/',
    component: Home
  },
  {
    path: '/login',
    component: Login
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
