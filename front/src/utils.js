export async function makeRequest(url, method, router, data = null, auth = true) {
    const headers = {
        'Content-Type': 'application/json'
    };

    if (auth) {
       const token = localStorage.getItem("jwt");
       if(!token){
           await router.push("/login");
           return
       }
       headers['Authorization'] = token;
    }

    const response = await fetch(url, {
        method: method,
        headers: headers,
        body: data ? JSON.stringify(data) : null

    });
    if (response.status === 401) {
        localStorage.setItem("jwt", '');
        await router.push("/login");
    } else if (response.status > 199 && response.status < 300) {
        await router.push("/home");
        return await response.json();
    } else {
        console.error('someting went wrong');
    }


}