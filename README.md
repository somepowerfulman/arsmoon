##Project php backend + Vue
####1. Copy config file
```
cd ENV_example.php ENV.php
```
####2. Build backend
```
docker-compose build
```
####3. Import Database dump
```
docker-compose run --rm db sh -c 'mysql -u user -h db -p testTask < /DBDumps/testTask_User.sql \
&& mysql -u user -h db -p testTask < /DBDumps/testTask_employeer.sql'

password : password
```

####4. Import Database dump
```
docker-compose up
```
####5. Install & run front
```
detailed instruction please find in front/README.md
```

####6. Test user:
```
login : foo@gmail.com,
password : 123456
```