<?php

use App\Helper\DBConnection;
use Firebase\JWT\JWT;

function getUserData($email, $conn)
{
    $table_name = 'User';
    $query = "SELECT id, email, first_name, last_name, password FROM " . $table_name . " WHERE email = ? LIMIT 0,1";
    $stmt = $conn->prepare($query);
    $stmt->bindParam(1, $email);
    $stmt->execute();
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    return $row;
}