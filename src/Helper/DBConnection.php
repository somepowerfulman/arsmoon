<?php


namespace App\Helper;



class DBConnection
{
    private $dbConnection;

    public function __construct($host, $port, $db_name, $user, $password, $encoding)
    {
        try {
            $this->dbConnection = new \PDO("mysql:host=$host;port=$port;dbname=$db_name",
            $user, $password,array(\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES '$encoding'"));
        }catch (\PDOException $e){
            exit($e->getMessage());
        }
    }

    public function getDBConnect()
    {
        return $this->dbConnection;
    }
}