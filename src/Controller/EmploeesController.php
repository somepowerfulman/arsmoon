<?php


namespace App\Controller;


use App\Model\EmploeesModel;

class EmploeesController extends BaseController
{

    public function model(){
        return new EmploeesModel($this->dbConnection);
    }

    public function index()
    {

        $responce = $this->model()->findAll($this->queryParams['limit'],
            $this->queryParams['offset'], $this->queryParams['query']);
        echo json_encode($responce);
    }

    public function create()
    {
        $postData = file_get_contents('php://input');
        $data = json_decode($postData, true);
        $response = $this->model()->pdoInsert($data);
        echo json_encode($response);
    }

    public function delete()
    {
        $response = $this->model()->delFomId($this->paramId);
        echo json_encode(['status' => 'ok']);
    }
}