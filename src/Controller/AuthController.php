<?php


namespace App\Controller;

use Firebase\JWT\JWT;
use PDO;

class AuthController
{
    private $dbConnection;
    private $requestMethod;

    public function __construct($dbConnection, $requestMethod)
    {
        $this->dbConnection = $dbConnection;
        $this->requestMethod = $requestMethod;
    }

    protected function notFound()
    {
        header("HTTP/1.1 404 Not Found");
        exit();
    }

    protected function notAuthentificated()
    {

        http_response_code(401);
        echo json_encode(array("message" => "Login failed. Either login or password is wrong"));
    }

    protected function getUserData($email)
    {
        $email = $email;
        $table_name = 'User';
        $conn = $this->dbConnection->getDBConnect();
        $query = "SELECT id, email, first_name, last_name, password FROM "
            . $table_name . " WHERE email = ? LIMIT 0,1";


        $stmt = $conn->prepare($query);
        $stmt->bindParam(1, $email);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

    protected function generateJwtFromUserData($userData)
    {
        $secret_key = "YOUR_SECRET_KEY";
        $issuer_claim = "THE_ISSUER"; // this can be the servername
        $issuedat_claim = time(); // issued at
        $expire_claim = $issuedat_claim + 3600; // expire time in seconds
        $token = array(
            "iss" => $issuer_claim,
            "iat" => $issuedat_claim,
            "exp" => $expire_claim,
            "data" => $userData);
        return ['jwt' => JWT::encode($token, $secret_key), 'expire_claim' => $expire_claim];
    }

    public function processRequest($id = null)
    {
        $this->requestMethod === 'POST' ? $this->login() : $this->notFound();
    }

    public function login()
    {
        $postData = file_get_contents('php://input');
        $data = json_decode($postData, true);
        $password = $data["password"];
        $email = $data["email"];

        $userData = $this->getUserData($data["email"]);

        if ($userData && password_verify($password, $userData['password'])) {
            $jwtData = $this->generateJwtFromUserData($userData);
            http_response_code(200);
            echo json_encode(
                array(
                    "message" => "Successful login.",
                    "jwt" => $jwtData['jwt'],
                    "email" => $email,
                    "expireAt" => $jwtData['expire_claim']
                ));
        } else {
            $this->notAuthentificated();
        }

    }
}