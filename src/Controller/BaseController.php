<?php


namespace App\Controller;



class BaseController
{
    protected $dbConnection;
    protected $tableName;
    private $requestMethod;
    /**
     * @var null
     */
    protected $queryParams;
    /**
     * @var null
     */
    private $id;

    public function __construct($dbConnection, $method, $paramId = null)
    {
        $this->dbConnection = $dbConnection;
        $this->requestMethod = $method;
        $this->paramId = $paramId;
        $this->queryParams = $_GET;
    }

    protected function notFound()
    {
        header("HTTP/1.1 404 Not Found");
        exit();
    }

    public function processRequest($id = null)
    {
        switch ($this->requestMethod) {
            case 'GET':
                (isset($id)) ? $this->show($id) : $this->index();
                break;
            case "POST":
                $this->create();
                break;
            case "DELETE":
                $this->delete();
                break;
            default:
                $this->notFound();
        }
    }

    protected function create()
    {
        $this->notFound();
    }

    protected function index()
    {
        $this->notFound();
    }

    protected function show($id)
    {
        $this->notFound();
    }

    protected function delete()
    {
        $this->notFound();
    }
}