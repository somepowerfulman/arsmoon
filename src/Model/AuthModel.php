<?php


namespace App\Model;


class AuthModel extends BaseModel
{
    protected $tableName = 'users';
    public function model(){
        return new AuthModel($this->dbConnection);
    }
}