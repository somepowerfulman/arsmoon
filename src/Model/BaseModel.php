<?php

namespace App\Model;

class BaseModel
{
    protected $DBConnection;

    public function __construct($DBConnection)
    {

        $this->DBConnection = $DBConnection;
    }

    public function findAll($page_size, $page_number, $query)
    {
        if ($page_number == 0) {
            $offset = $page_number;
        } else {
            $offset = ($page_size * $page_number);
        }
        $limit = $page_size + 1;
        $statement = $this->getFindAllQuery($limit, $offset, $query);
        $result = $this->runQuery($statement);
        return $result;
    }

    public function runQuery(string $statement, array $query_args = null)
    {
        try {
            $statement = $this->DBConnection->getDBConnect()->prepare($statement);
            $statement->execute($query_args);
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function delFomId($id)
    {
        $sql = "DELETE FROM $this->tableName WHERE id = $id;";
        $sql = $this->DBConnection->getDBConnect()->prepare($sql);
        $sql = $sql->execute();
        return $sql;
    }


}