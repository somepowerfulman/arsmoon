<?php
require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../ENV.php');

use App\Controller\AuthController;
use App\Controller\EmploeesController;
use App\Helper\DBConnection;
use \Firebase\JWT\JWT;

$dbConnection = new DBConnection(DB_HOST, DB_PORT,
    DB_NAME, DB_USER, DB_PASSWORD, DB_ENCODING);
require(__DIR__ . '/../src/Helper/jwtAuth.php');
//require(__DIR__ . '/../src/Helper/jwtreg.php');

$id = null;
$requestMethod = $_SERVER["REQUEST_METHOD"];
$uri = trim($_SERVER["REQUEST_URI"], "/");
$parsed_url = parse_url($uri);
$url = explode("/", $parsed_url["path"]);
$query_params = getQueryPasramsFromUrl($parsed_url);

function getQueryPasramsFromUrl($parsed_url)
{
    if (!empty($parsed_url['query'])) {
        parse_str($parsed_url['query'], $query_params);
        return $query_params;
    }
    return false;
}

function isLoggedIn($dbConnection, $jwt)
{
    $secret_key = "YOUR_SECRET_KEY";
    $conn = $dbConnection->getDBConnect();
    try {
        $jwtData = JWT::decode($jwt, $secret_key, array('HS256'));
        $jwtDataArray = json_decode(json_encode($jwtData), true);
        $userData = getUserData($jwtDataArray['data']["email"], $conn);
        if ($userData && ($jwtDataArray['data']["password"] === $userData['password'])) {
            return true;
        }

    } catch (Exception $e) {
    }
    return false;
}
$_id = isset($url[3]) ? $url[3] : null;
$allHeaders = getallheaders();
if (isset($url[2])) {
    switch ($url[2]) {
        case "employees":
            if (array_key_exists("Authorization", $allHeaders)
                && isLoggedIn($dbConnection, $allHeaders["Authorization"])) {
                $controller = new EmploeesController($dbConnection, $requestMethod, $_id);
            } else {
                header("HTTP/1.1 401 Unauthorized");
                exit();
            }

            break;

        case "login":
            $controller = new AuthController($dbConnection, $requestMethod);
            break;
        default:
            header("HTTP/1.1 404 Not Found");
            exit();
    }
} else {
    header("HTTP/1.1 404 Not Found");
    exit();
}

$controller->processRequest($id);